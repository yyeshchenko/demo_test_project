### The demo project, that includes both UI and API tests with integration to gitlab CI

#### Description:
a. To run tests locally, use the following command: 
  gradlew clean test allureReport
  
    After successful run find the generated report in 
  /build/reports/allure-report/index.html

b. To run the tests on CI/CD - go to
  https://gitlab.com/yyeshchenko/demo_test_project/-/pipelines
  1) Click on Run pipeline on the right top corner
  2) When job passed, navigate to it -> automation_tests -> click on Browse btn 
  on open job_artifacts 
  3) To observe the generated report for Gitlab job: go to build/reports/allure-report/index.html

c. Link to Calliope.pro: https://app.calliope.pro/profiles/4379/reports 

d. improvement for Calliope.pro: among others supportive test report formats - to support 
  Allure format as well
   feature: to be able to support html format, as well as xml

e, f. to select the scenarios:
   for API tests: the most critical - to create user and login, and create pet and to be able to work with it
   for UI tests: the most popular and used - to explain workig with inputs, buttons and waits

g. next steps for the project:
- implement swagger for test framework to test schema as well, or to implement schema validation
- cover other endpoints (place order, upload images, etc)   
- realize parallel tests running
- cross-browser running for UI tests
